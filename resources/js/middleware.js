import store from './store/index';

export function requireAuth(to, from, next) {

    if (!store.getters.isAuthorized) {

        next({
            path: '/login',
            query: { redirect: to.fullPath }
        })
    } else {
        next()
    }

    next();
}

export function requireGuest(to, from, next) {

    if (store.getters.isAuthorized) {
        next({
            path: '/'
        })
    } else {
        next()
    }

    next();
}
