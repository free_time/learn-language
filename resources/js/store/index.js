import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

const state = {
    user: null,
	authorized: false,
};

const getters = {
    getUser: state => state.user,
	isAuthorized: state => state.authorized,
};

const mutations = {
    setUser: (state, payload) => {state.user = payload},
};

const actions = {
};

export default new Vuex.Store({
    state,
    actions,
    getters,
    mutations,
    strict: debug
});

