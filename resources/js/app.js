/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import validatorUa from 'vee-validate/dist/locale/uk';
import {routes} from './router';
import {checkAuth} from './auth';
import {authMixin} from './mixins';
import {deleteCookie} from "./helper";
import store from './store/index';

window.Vue = require('vue');
Vue.mixin(authMixin);
Vue.use(VueRouter);

Vue.use(VeeValidate, {
    classes: true,
    locale: 'ua',
    dictionary: {validatorUa},
    classNames: {
        invalid: 'is-invalid',
        valid: 'is-valid'
    }
});

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: routes
});

axios.defaults.baseURL = 'http://learn-language.com';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

checkAuth().then(user => {

    if(user) {
        sessionStorage.setItem('user', JSON.stringify(user))
    } else {
        deleteCookie('auth');
        deleteCookie('expired_at');
        sessionStorage.removeItem('user');
        user = null;
    }

    new Vue({
        store,
        el: '#app',
        router,
        created() {
            if(user && user.data) {
                this.$store.commit('setUser', user.data);
            }
        },
        mounted() {
            document.getElementById('app-preload').classList.add('disabled')
        }
    });

}).catch(null, error => {
    console.log('error', error);
});
