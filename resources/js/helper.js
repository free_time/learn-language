export function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : null;
}

export function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

export function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}

/**
 * process axios error response using VeeValidate
 *
 * @param self
 * @param errors
 */
export function errorsHandler(self, errors) {
    self.loading = false;
    if(errors.response.data.errors) {
        Object.keys(errors.response.data.errors).forEach(key => {

            const error = errors.response.data.errors[key];
            const field = self.$validator.fields.find({name: key, scope: self.$options.scope});

            if(field.id) {

                self.$validator.errors.add({
                    id: field.id,
                    field: key,
                    msg: error.join(' '),
                    scope: self.$options.scope,
                });

                field.setFlags({
                    invalid: true,
                    valid: false,
                    validated: true,
                });
            } else {
                console.log('field ' + key + ' not found.')
            }

        });
    }
}
