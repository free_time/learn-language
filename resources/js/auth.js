export function checkAuth() {
    return axios.get('/api/user')
        .then(response => {
            return response.data;
        })
        .catch(error => {
        	console.log(error);
            if(error.response.status === 401) {
                return null
            } else {
                return error;
            }
        });
}
