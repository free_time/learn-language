export const auth = {
    email   : 'Емаіл адрес',
    password: 'Пароль',
    login   : 'Увійти',
    logout  : 'Вийти',
	name    : 'Ім\'я',
	last_name   : 'Прізвище',
	login_form	: 'Форма входу',
	conf_password   : 'Повторіть пароль',
    registration    : 'Реєстрація',
    forgetPassword  : 'Забули пароль',
};
export const appName = 'Learn Language';
