import store from './store/index';
import {deleteCookie, setCookie} from "./helper";
import {errorsHandler} from './helper';

export const authMixin = {
    data: function () {
        return {
            loading: false
        }
    },
    methods: {
        authLogin: function (isValid) {
            if (isValid) {
                this.loading = true;
                axios.post('/api/oauth/token',
					{
						username: this.username,
						password: this.password,
						client_secret: 'XBVjxwExSAMRp8H4VJEZtlc10BKjrNbKhRRjuBo4',
						client_id: 2,
						grant_type: 'password'
					}).then(response => {
                    this.loading = false;
                    store.commit('setAuthorized', true);
                    axios.defaults.headers.common['Authorization'] = `Bearer ${ response.data.access_token }`;

                    console.log('axios.defaults', axios.defaults);

                    setCookie('auth', response.data.access_token);
                    setCookie('expired_at', response.data.expired_at);
                    sessionStorage.setItem('user', JSON.stringify(response.data.user));
                    this.$store.commit('setToken', response.data.access_token);
                    this.$store.commit('setUser', response.data.user);

                    this.$router.push('/');

                }).catch((errors) => {errorsHandler(this, errors)});
            }
        },
        authLogout: function () {
            axios.post('/api/logout').then(response => {
                if(response.data.success) {
                    deleteCookie('auth');
                    deleteCookie('expired_at');
                    this.$store.commit('setToken', null);
                    this.$store.commit('setUser', null);
                    store.commit('setAuthorized', false);

                    this.$router.push('/login');
                }
            });
        },
        passwordForgot: function (isValid) {
          if(isValid) {
              axios.post('/api/password/forget', { email: this.email }).then(response => {
                  this.loading = false;

                  console.log('response', response);
                  alert('Done');

              }).catch((errors) => {errorsHandler(this, errors)});
          }
        },
        passwordReset: function (isValid) {
            if(isValid) {
                axios.post('/api/password/reset', {
                    token: this.token,
                    email: this.email,
                    password: this.password,
                    password_confirmation: this.password_confirmation
                }).then(response => {
                    this.loading = false;

                    console.log('response', response);
                    alert('Done');

                }).catch((errors) => {errorsHandler(this, errors)});
            }
        },
        registration: function (isValid) {
            if(isValid) {
                axios.post('/api/register', {
                    name: this.name,
                    email: this.email,
                    password: this.password,
                }).then(response => {
                    this.loading = false;

                    console.log('response', response);
                    alert('Done');

                }).catch((errors) => {errorsHandler(this, errors)});
            }
        },
    }
};
