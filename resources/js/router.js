import Home from './pages/Home';

import LoginPage from './pages/auth/Login';

// profile
import {requireAuth, requireGuest} from './middleware';

export const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'login',
        component: LoginPage,
        beforeEnter: requireGuest,
    },
];
