<?php

namespace App\Http\Middleware\Api;

use Closure;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (strpos($request->route()->getName(),'passport.token')!==false){
            resolve(\App\Http\Requests\Auth\Login::class);
        }
        return $next($request);
    }
}
